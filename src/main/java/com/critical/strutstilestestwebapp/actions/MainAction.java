package com.critical.strutstilestestwebapp.actions;

import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author nadeev.sa
 */

public class MainAction extends ActionSupport {

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    
}
